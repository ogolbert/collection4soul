﻿using System.ComponentModel;
using Golbert.WPFHelper;
using ColbertEnums = Golbert.Collection4Soul.Utilities.Enums;
using System.Windows.Input;
using System;
using System.Windows.Controls;


namespace Golbert.Collection4Soul.Clients.ViewModel
{
    public class Collection4SoulVM : ViewModelBase, ICloneable
    {

        #region module variables

        int _ID;
        string _Name;
        string _Author;
        int _CreateYear;
        string _Producer;
        int _ProduceYear;
        string _Remark;
        decimal _Price;
        decimal _PriceMarket;
        ColbertEnums.ConditionOfExemplars _Condition;
        ColbertEnums.KindOfExemplar _Kind;
        //ColbertEnums.MultimediaMediaAudio _MediaAudio;
        //ColbertEnums.MultimediaMediaVideo _MediaVideo;

        //Collection4SoulVM _beforChanging; //4 rollback if exit after change

        //bool _isChanged = false;

        //bool _isEnabledVM = false;

        #endregion module variables

        #region constructors


        public Collection4SoulVM()
        {

        }

        #endregion constructors

        #region events

        public event EventHandler<ExemplarEditingEventArgs> IsEditing;
        public event EventHandler<ErrorEventArgs> ErrorFromExemplarVM;

        #endregion

        #region COMMANDS

        #region command TextBox KeyUp

        ICommand _commandTextBoxKeyUp;

        public ICommand CommandTextBoxKeyUp
        {
            get
            {
                try
                {
                    _commandTextBoxKeyUp = _commandTextBoxKeyUp ?? (_commandTextBoxKeyUp = new DelegateCommand((a) => TextBoxChanged(a)));
                }
                catch (Exception ex)
                {
                    ErrorFromExemplarVM(this, new ErrorEventArgs(ex));
                }
                return _commandTextBoxKeyUp;
            }
        }

        void TextBoxChanged(object control)
        {
            try
            {
                TextBox box = control as TextBox;
                IsEditing(this, new ExemplarEditingEventArgs(this.GetType().GetProperty(box.GetBindingExpression(TextBox.TextProperty).ResolvedSourcePropertyName)));
            }
            catch (Exception ex)
            {
                ErrorFromExemplarVM(this, new ErrorEventArgs(ex));
            }
        }

        #endregion command TextBox KeyUp

        #region command combobox selection changed

        ICommand _commandComboBoxSelectionChanged;

        public ICommand CommandComboBoxSelectionChanged
        {
            get
            {
                try
                {
                    _commandComboBoxSelectionChanged = (_commandComboBoxSelectionChanged = new DelegateCommand((a) => ComboBoxSelectionChanged(a)));
                }
                catch (Exception ex)
                {
                    ErrorFromExemplarVM(this, new ErrorEventArgs(ex));
                }
                return _commandComboBoxSelectionChanged;
            }
        }

        void ComboBoxSelectionChanged(object control)
        {
            try
            {
                ComboBox combo = control as ComboBox;
                if (combo.GetBindingExpression(ComboBox.SelectedItemProperty).ResolvedSourcePropertyName != null)
                {
                    IsEditing(this, new ExemplarEditingEventArgs(this.GetType().
                        GetProperty(combo.GetBindingExpression(ComboBox.SelectedItemProperty).ResolvedSourcePropertyName)));
                }
            }
            catch (Exception ex)
            {
                ErrorFromExemplarVM(this, new ErrorEventArgs(ex));
            }
        }


        #endregion command combobox selection changed

        #endregion COMMANDS

        #region public properties


        //public bool IsEnabledVM
        //{
        //    set
        //    {
        //        _isEnabledVM = value;
        //        OnPropertyChanged();
        //    }
        //    get
        //    {
        //        return _isEnabledVM;

        //    }
        //}

        public int ID
        {
            set
            {
                _ID = value;
                OnPropertyChanged();
            }
            get
            {
                return _ID;
            }
        }

        public string Name
        {
            set
            {
                _Name = value;
                OnPropertyChanged();

            }
            get
            {
                return _Name;
            }
        }

        public string Author
        {
            set
            {
                _Author = value;
                OnPropertyChanged();
            }
            get
            {
                return _Author;
            }
        }

        public int CreateYear
        {
            set
            {
                _CreateYear = value;
                OnPropertyChanged();
            }
            get
            {
                return _CreateYear;
            }
        }

        public string Producer
        {
            set
            {
                _Producer = value;
                OnPropertyChanged();
            }
            get
            {
                return _Producer;
            }
        }

        public int ProduceYear
        {
            set
            {
                _ProduceYear = value;
                OnPropertyChanged();
            }
            get
            {
                return _ProduceYear;
            }
        }

        public string Remark
        {
            set
            {
                _Remark = value;
                OnPropertyChanged();
            }
            get
            {
                return _Remark;
            }
        }

        public decimal Price
        {
            set
            {
                _Price = value;
                OnPropertyChanged();

            }
            get
            {
                return _Price;
            }
        }

        public decimal PriceMarket
        {
            set
            {
                _PriceMarket = value;
                OnPropertyChanged();
            }
            get
            {
                return _PriceMarket;
            }
        }

        public ColbertEnums.ConditionOfExemplars Condition
        {
            set
            {
                _Condition = value;
                OnPropertyChanged();
            }
            get
            {
                return _Condition;
            }
        }

        public ColbertEnums.KindOfExemplar Kind
        {
            set
            {
                _Kind = value;
                OnPropertyChanged();
            }
            get
            {
                return _Kind;
            }
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        //public Collection4Soul.Utilities.Enums.MultimediaMediaAudio MediaAudio
        //{
        //    set
        //    {
        //        _MediaAudio = value;
        //OnPropertyChanged();
        //    }
        //    get
        //    {
        //        return _MediaAudio;
        //    }
        //}

        //public Collection4Soul.Utilities.Enums.MultimediaMediaVideo MediaVideo
        //{
        //    set
        //    {
        //        _MediaVideo = value;
        //  OnPropertyChanged();
        //    }
        //    get
        //    {
        //        return _MediaVideo;
        //    }
        //}

        #endregion public properties
    }


}
