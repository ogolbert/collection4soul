﻿using System;
using System.Windows.Input;
using System.ServiceModel;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using System.IO;

//using System.Diagnostics;
//using System.Runtime.CompilerServices;

using Golbert.WPFHelper;
using GolbertUtilities = Golbert.Collection4Soul.Utilities;



namespace Golbert.Collection4Soul.Clients.ViewModel
{
    public class Collection4SoulViewVM : ViewModelBase
    {
        #region module declarations

        string _logPath;

        Collection4SoulVM _ExemplarVM;
        Collection4SoulVM _ExemplarVMBackUp; //4 rollback if exit after change

        ObservableCollection<Collection4SoulVM> _ListOfExemplarsVM = new ObservableCollection<Collection4SoulVM>();
        ObservableCollection<Collection4SoulVM> _RawListOfExemplarsVM = new ObservableCollection<Collection4SoulVM>();

        string _FilterText = "";

        bool _isEditing = false;

        ChannelFactory<Golbert.Collection4Soul.Clients.ViewModel.Collection4SoulTheService.ICollection4SoulContractChannel> _factory;

        List<PropertyInfo> _ListOfEdited;

        #endregion module declarations

        #region constructors

        public Collection4SoulViewVM()
        {
            //createLog();
            try
            { 
                _factory = new ChannelFactory<Golbert.Collection4Soul.Clients.ViewModel.Collection4SoulTheService.ICollection4SoulContractChannel>("NetTcpBinding_ICollection4SoulContract");

                _RawListOfExemplarsVM = FillCollection(); //full collection from datasource
                _ListOfExemplarsVM = FillCollection(); //manipulating collection, in constructor the same first

                _ListOfEdited = new List<PropertyInfo>(); //List of chenged properties, control of enabling

                int i = int.Parse("m");
            }
            catch(Exception ex)
            {
                //ErrorFromMainVM(this, new ErrorEventArgs(ex));
                RaiseError(ex);
               
            }
        }


        #endregion constructors

        #region events

        public event EventHandler<ErrorEventArgs> ErrorFromMainVM;

        void RaiseError(Exception ex)
        {
            if (ErrorFromMainVM != null) ErrorFromMainVM(this, new ErrorEventArgs(ex));
        }

        #endregion events


        #region COMMANDS


        //#region change selection command

        //ICommand _commandListViewItemSelected;

        //public ICommand CommandListViewItemSelected
        //{
        //    get
        //    {
        //        return _commandListViewItemSelected ?? (_commandListViewItemSelected = new DelegateCommand((a) => SelectionChanged(a)));
        //    }
        //}

        //private void SelectionChanged(object param)
        //{
        //    //ExemplarVM.IsEnabledVM = false;
        //}

        //#endregion change selection command

        //#region textbox changed

        ////ICommand _commandTextBoxChanged;

        ////public ICommand CommandTextBoxChanged
        ////{
        ////    get
        ////    {
        ////        return _commandTextBoxChanged ?? (_commandTextBoxChanged = new DelegateCommand((a) => Console.WriteLine(a.ToString())));
        ////    }
        ////}


        //#endregion textbox changed

        //#region textbox key up

        ////ICommand _commandTextBoxKeyUp;

        ////public ICommand CommandTextBoxChanged
        ////{
        ////    get
        ////    {
        ////        return _commandTextBoxKeyUp ?? (_commandTextBoxKeyUp = new DelegateCommand((a) => Console.WriteLine(a.ToString())));
        ////    }
        ////}


        //#endregion textbox key up

        #region new exemplar command

        DelegateCommand _commandNewExemplar;

        public DelegateCommand CommandNewExemplar
        {
            get
            {
                return _commandNewExemplar ?? (_commandNewExemplar = new DelegateCommand(commandNewExemplarExecute, commandNewExemplarCanExecute));
            }
        }

        void commandNewExemplarExecute(object param)
        {
            try
            {
                ExemplarVM = new Collection4SoulVM();
                IsEditing = true;
                IsNew = true;
                RefreshCommand4Buttons();
            }
            catch (Exception ex)
            {
                //ErrorFromMainVM(this, new ErrorEventArgs(ex));
                RaiseError(ex);
            }
        }

        bool commandNewExemplarCanExecute(object param)
        {
            return !IsEditing;
        }

        #endregion new exemplar command

        #region save exemplar command

        DelegateCommand _commandSaveExemplar;

        public DelegateCommand CommandSaveExemplar
        {
            get
            {
                try
                {
                    _commandSaveExemplar = _commandSaveExemplar ?? (_commandSaveExemplar = new DelegateCommand(commandSaveExecute, commandExitCanExecute));
                }
                catch (Exception ex)
                {
                    //ErrorFromMainVM(this, new ErrorEventArgs(ex));
                    RaiseError(ex);
                }
                return _commandSaveExemplar;
            }
        }

        void commandSaveExecute(object param)
        {
            try
            {
                string xml = GolbertUtilities.InternalCommonUsing.Serialize(_ExemplarVM);
                if (sendObjectData2Service(xml))
                {
                    if (IsNew)
                    {
                        _ListOfExemplarsVM.Add(_ExemplarVM);
                        _RawListOfExemplarsVM.Add(_ExemplarVM);
                    }
                    else
                    {
                        _ExemplarVMBackUp = CreateClon(_ExemplarVM);
                    }
                }
                SetNotEditCondition();
            }
            catch (Exception ex)
            {
                //ErrorFromMainVM(this, new ErrorEventArgs(ex));
                RaiseError(ex);

            }
        }

        bool commandSaveCanExecute()
        {
            return IsEditing;
        }

        #endregion save exemplar command

        #region exit command

        DelegateCommand _commandExit;

        public DelegateCommand CommandExit
        {
            get
            {
                try
                {
                    _commandExit = _commandExit ?? (_commandExit = new DelegateCommand(commandExitExecute, commandExitCanExecute));
                }
                catch (Exception ex)
                {
                    //ErrorFromMainVM(this, new ErrorEventArgs(ex));
                    RaiseError(ex);
                }
                return _commandExit;
            }
        }

        void commandExitExecute(object param)
        {
            try
            {
                ExemplarVM = _ExemplarVMBackUp;
                SetNotEditCondition();
            }
            catch (Exception ex)
            {
                //ErrorFromMainVM(this, new ErrorEventArgs(ex));
                RaiseError(ex);
            }
        }

        bool commandExitCanExecute(object param)
        {
            return IsEditing;
        }

        #endregion exit command

        //#region getList command

        //ICommand _commandGetList; //not used

        //public string ButtonContentGetList
        //{
        //    get
        //    {
        //        return "GetList";
        //    }
        //}

        //public ICommand CommandGetList
        //{
        //    get
        //    {
        //        return _commandGetList ?? (_commandGetList = new DelegateCommand((a) =>
        //        {
        //            try
        //            {
        //                _ListOfExemplarsVM = FillCollection();
        //            }
        //            catch (Exception ex)
        //            {
        //                Console.WriteLine(ex.StackTrace + " ::: " + ex.Message);
        //            }
        //        }));
        //    }
        //}

        //#endregion getList command

        #endregion COMMANDS

        #region local methods

        ObservableCollection<Collection4SoulVM> FillCollection()
        {
            ObservableCollection<Collection4SoulVM> col = new ObservableCollection<Collection4SoulVM>();
            try
            {
                string[] xml = requestListFromService();
                foreach (var x in xml)
                {
                    Collection4SoulVM item = new Collection4SoulVM();
                    GolbertUtilities.InternalCommonUsing.CreateObjFromXML<Collection4SoulVM>(x, item);
                    col.Add(item);
                }
            }
            catch (Exception ex)
            {
                //ErrorFromMainVM(this, new ErrorEventArgs(ex));
                RaiseError(ex);
            }
            return col;
        }

        void SetNotEditCondition()
        {
            try
            {
                _ListOfEdited.Clear();
                IsEditing = false;
                RefreshCommand4Buttons();
            }
            catch (Exception ex)
            {
                //ErrorFromMainVM(this, new ErrorEventArgs(ex));
                RaiseError(ex);
            }
        }

        void RefreshCommand4Buttons()
        {
            try
            {
                _commandExit.RaiseCanExecuteChanges();
                _commandNewExemplar.RaiseCanExecuteChanges();
                _commandSaveExemplar.RaiseCanExecuteChanges();
            }
            catch (Exception ex)
            {
                //ErrorFromMainVM(this, new ErrorEventArgs(ex));
                RaiseError(ex);
            }
        }

        #endregion local methods


        #region properites

        public ChannelFactory<Golbert.Collection4Soul.Clients.ViewModel.Collection4SoulTheService.ICollection4SoulContractChannel> Factory
        {
            set
            {
                _factory = value;
            }
        }

        Collection4SoulVM CreateClon (Collection4SoulVM first)
        {
            Collection4SoulVM second = new Collection4SoulVM();
            try
            {
                if (first != null) second = (Collection4SoulVM)first.Clone();
            }
            catch (Exception ex)
            {
                //ErrorFromMainVM(this, new ErrorEventArgs(ex));
                RaiseError(ex);
            }
            return second;

        }

        public Collection4SoulVM ExemplarVM
        {
            set
            {
                try
                {
                    _ExemplarVM = value;
                    if (_ExemplarVM != null)
                    {
                        _ExemplarVMBackUp = CreateClon(_ExemplarVM);
                        _ExemplarVM.IsEditing += _ExemplarVM_IsEditing;
                        _ExemplarVM.ErrorFromExemplarVM += _ExemplarVM_ErrorFromExemplarVM;
                    }
                    OnPropertyChanged();
                }
                catch(Exception ex)
                {
                    //ErrorFromMainVM(this, new ErrorEventArgs(ex));
                    RaiseError(ex);
                }
            }
            get
            {
                return _ExemplarVM;
            }
        }

        private void _ExemplarVM_ErrorFromExemplarVM(object sender, ErrorEventArgs e)
        {
            ErrorFromMainVM(this, new ErrorEventArgs(e.ErrorData));
        }

        private void _ExemplarVM_IsEditing(object sender, ExemplarEditingEventArgs e)
        {
            if (IsNew) return;
            try
            {
                PropertyInfo epi = e.PropertyEdited;
                string epin = epi.Name;
                object epiv = epi.GetValue(_ExemplarVM);

                PropertyInfo opi = typeof(Collection4SoulVM).GetProperty(epin);
                object opiv = opi.GetValue(_ExemplarVMBackUp);


                if (epiv.ToString() != opiv.ToString())
                {
                    if (!_ListOfEdited.Exists(a => a.Name == epi.Name)) _ListOfEdited.Add(epi);
                }
                else
                {
                    _ListOfEdited.Remove(epi);
                }
                IsEditing = _ListOfEdited.Count > 0;
                RefreshCommand4Buttons();
            }
            catch (Exception ex)
            {
                //ErrorFromMainVM(this, new ErrorEventArgs(ex));
                RaiseError(ex);
            }

        }


        public ObservableCollection<Collection4SoulVM> ListOfExemplarsVM
        {
            set
            {
                _ListOfExemplarsVM = value;
                OnPropertyChanged();
            }
            get
            {
                return _ListOfExemplarsVM;
            }
        }

        public bool IsEditing
        {
            set
            {
                _isEditing = value;
                OnPropertyChanged();
            }
            get
            {
                return _isEditing;
            }
        }

        public bool IsNew
        {
            private set; get;
        }

        public string FilterText
        {
            set
            {
                _FilterText = value;
                DoFilter();
            }
            get
            {
                return _FilterText;
            }
        }

        void DoFilter()
        {
            try
            {
                var col = _RawListOfExemplarsVM.Where((a) => a.Name.Contains(_FilterText));
                _ListOfExemplarsVM.Clear();
                foreach (var item in col)
                {
                    _ListOfExemplarsVM.Add(item);
                }
            }
            catch (Exception ex)
            {
                //ErrorFromMainVM(this, new ErrorEventArgs(ex));
                RaiseError(ex);
            }
        }



        #endregion properties



        #region requests to service


        bool sendObjectData2Service(string xml)
        {
            bool check = false;
            try
            {
                Collection4SoulTheService.ICollection4SoulContract channel = _factory.CreateChannel();
                if (IsNew)
                {
                    int newId = channel.InsertExemplar(xml);

                    if (newId != -999)
                    {
                        _ExemplarVM.ID = newId;
                        check = true;
                    }
                    else Console.WriteLine("Insert failed");
                }
                else
                {
                    if (channel.UpdateExemplar(xml))
                    {
                        check = true;
                    }
                    else Console.WriteLine("Update failed");
                }
            }
            catch (Exception ex)
            {
                //ErrorFromMainVM(this, new ErrorEventArgs(ex));
                RaiseError(ex);
            }
            return check;
        }

        string[] requestListFromService()
        {
            try
            {
                Collection4SoulTheService.ICollection4SoulContract channel = _factory.CreateChannel();
                return channel.GetExemplars();
            }
            catch (Exception ex)
            {
                //ErrorFromMainVM(this, new ErrorEventArgs(ex));
                RaiseError(ex);
                throw new NotImplementedException();
            }
        }

        #endregion requests to service

        #region logging

        //void createLog()
        //{
        //    string directoryPath = System.Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Collection4SoulLogs\\";
        //    if (!Directory.Exists(directoryPath)) Directory.CreateDirectory(directoryPath);

        //    _logPath = directoryPath + System.DateTime.Today.ToString("yyyyMMdd") + System.DateTime.Now.ToString("HHmmss") + "_" + typeof(Collection4SoulViewVM).Assembly.FullName + ".log";

        //    writeLog("Started without errors " + System.DateTime.Today.ToString("yyyy.MM.dd") + " at " + System.DateTime.Now.ToString("HH:mm:ss"));
        //}

        //void writeLog(string info)
        //{
        //    using (StreamWriter writer = File.AppendText(_logPath))
        //    {
        //        writer.WriteLine(info);
        //    }
        //}
        //void writeLog (Exception exeption)
        //{
        //    using (StreamWriter writer = File.AppendText(_logPath))
        //    {
        //        writer.WriteLine(">>>Occured: " + System.DateTime.Today.ToString("yyyy.MM.dd") + " at " + System.DateTime.Now.ToString("HH:mm:ss"));
        //        writer.WriteLine(">>Message: " + exeption.GetBaseException().Message.ToString());
        //        if (exeption.InnerException != null) writer.WriteLine(">>InnerExeption: " + exeption.InnerException.Message.ToString());
        //        writer.WriteLine(">>StackTrace: " + exeption.StackTrace.ToString());
        //    }
        //}

        #endregion logging

    }

}
