﻿using System;
using System.Reflection;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Golbert.Collection4Soul.Clients.ViewModel
{
    public class EditingEventArgs : EventArgs
    {
        public EditingEventArgs(bool edited)
        {
            isEdited = edited;
        }

        public bool isEdited { private set; get; }
    }

    public class ExemplarEditingEventArgs : EventArgs
    {
        public ExemplarEditingEventArgs(PropertyInfo property)
        {
            PropertyEdited = property;
        }

        public PropertyInfo PropertyEdited { private set; get; }
    }

    public class ErrorEventArgs : EventArgs
    {
        public ErrorEventArgs(Exception exeption)
        {
            ErrorData = exeption;
        }

        public Exception ErrorData { private set; get; }
    }

    //public class ComboBoxSelectionChangedEventArgs : EventArgs
    //{
    //    public ComboBoxSelectionChangedEventArgs(object oldValue)
    //    {
    //        PreviousItemSelectedValue = oldValue;
    //    }

    //    public object PreviousItemSelectedValue { private set; get; }
    //}

    //public class ComboBoxSelectionChangedEventArgs<T> : EventArgs
    //{
    //    public ComboBoxSelectionChangedEventArgs(T type, object oldValue)
    //    {
    //        PreviousItemSelectedValue = oldValue;
    //    }

    //    public T PreviousItemSelectedType { private set; get; }
    //    public object PreviousItemSelectedValue { private set; get; }
    //}


}
