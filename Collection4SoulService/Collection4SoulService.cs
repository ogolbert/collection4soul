﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Runtime.Serialization;
using Golbert.Collection4Soul.Contract;
using Golbert.Collection4Soul.Utilities;
using System.Reflection;
using System.Configuration;
//using Golbert.Collection4Soul;



namespace Golbert.Collection4Soul.Service
{
    public class Collection4SoulService : ICollection4SoulContract
    {

        public string[] GetExemplars()
        {
            ListOfExemplars col = new ListOfExemplars();
            List<string> xmlList = Golbert.Collection4Soul.Utilities.InternalCommonUsing.SerializeList<Exemplar>(col);
            string[] xml = xmlList.ToArray();

            return xml;
        }

        public bool UpdateExemplar(string xml)
        {
            bool check = false;
            try
            {
                Exemplar exempl = new Exemplar();
                Golbert.Collection4Soul.Utilities.InternalCommonUsing.CreateObjFromXML(xml, exempl);

                check = exempl.Update(exempl);
            }
            catch (Exception ex)
            {
                check = false;
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (check) Console.WriteLine("from Service: Victory! Saving successfull");
                else Console.WriteLine("from Service: Defeat... Saving failed");
            }
            return check;
        }

        public int InsertExemplar(string xml)
        {
            bool check = false;
            int newID = 0;
            try
            {
                Exemplar exempl = new Exemplar();
                exempl.IsNew = true;
                Golbert.Collection4Soul.Utilities.InternalCommonUsing.CreateObjFromXML(xml, exempl);

                newID = exempl.Insert(exempl);
                check = true;
            }
            catch (Exception ex)
            {
                check = false;
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (check) Console.WriteLine("from Service: Victory! Saving successfull");
                else Console.WriteLine("from Service: Defeat... Saving failed");
            }
            return newID;
        }


        public XmlSerializer ReadExemplar(int id)
        {
            throw new NotImplementedException();
        }
    }
}
