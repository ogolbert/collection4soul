﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Controls;
using System.Reflection;
using Golbert.WPFHelper;


namespace Golbert.WPFHelper.Converters
{
    public class TurnBoolean : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !(bool)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value;
        }
    }

    public class EditingtoVisibility : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool)parameter)
            {
                if ((bool)value) return Visibility.Visible;
                else return Visibility.Hidden;
            }
            else
            {
                if ((bool)value) return Visibility.Hidden;
                else return Visibility.Visible;
            }

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool)value) return Visibility.Hidden;
            else return Visibility.Visible;
        }
    }

    //public class ConverterIsNewToEditColor : IValueConverter
    //{
    //    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        if ((bool)value) return Brushes.Orchid;
    //        else return Brushes.Transparent;
    //    }

    //    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        return (bool)value;
    //    }
    //}

    public class CaptionDependingFromCondition : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(bool)value)
            {
                return "Create new Exemplar";
            }
            else
            {
                return "Enabled after saving";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value;
        }
    }

    public class ConverterTextValue2Visability : IValueConverter
    {
        //this converter is not using
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Visibility vis = Visibility.Hidden;
            bool switcher = false;
            try
            {
                switch (parameter.ToString())
                {
                    case "MediaAudioVisible":
                        switcher = (value.ToString() == "Music");
                        break;
                    case "MediaVideoVisible":
                        switcher = (value.ToString() == "Video");
                        break;
                    default:
                        switcher = false;
                        break;
                }

                if (switcher) vis = Visibility.Visible;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            };
            return vis;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}


