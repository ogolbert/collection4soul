﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace Golbert.WPFHelper.Behaviors
{

    public static class TextBoxBehaviors
    {

        #region TextBox Changed
        public static readonly DependencyProperty TextChangedCommandProperty =
            DependencyProperty.RegisterAttached("TextChanged",
            typeof(ICommand),
            typeof(TextBoxBehaviors),
            new UIPropertyMetadata(null, new PropertyChangedCallback(TextBoxBehaviors.TextChangedChanged)));


        public static void SetTextChanged(TextBox target, ICommand value)
        {
            target.SetValue(TextBoxBehaviors.TextChangedCommandProperty, value);
        }

        public static ICommand GetTextChanged(TextBox target)
        {
            return (ICommand)target.GetValue(TextBoxBehaviors.TextChangedCommandProperty);
        }

        private static void TextChangedChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            TextBox element = target as TextBox;
            if (element != null)
            {
               element.TextChanged += Element_TextChanged; 
            }
            else
            {
                element.TextChanged -= Element_TextChanged;
            }
        }

        private static void Element_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox current = (TextBox)sender;
            BindingExpression be = current.GetBindingExpression(TextBox.TextProperty);

            if (be != null) be.UpdateSource();

            ICommand command = GetTextChanged(current);

            if (command.CanExecute(null))
                command.Execute(current);
        }

        #endregion TextBox Changed

        #region TextBox KeyUp

        public static readonly DependencyProperty KeyUpCommandProperty =
                DependencyProperty.RegisterAttached("KeyUp",
                typeof(ICommand),
                typeof(TextBoxBehaviors),
                new UIPropertyMetadata(null, new PropertyChangedCallback(TextBoxBehaviors.KeyUpChanged)));


        public static void SetKeyUp(TextBox target, ICommand value)
        {
            target.SetValue(TextBoxBehaviors.KeyUpCommandProperty, value);
        }

        public static ICommand GetKeyUp(TextBox target)
        {
            return (ICommand)target.GetValue(TextBoxBehaviors.KeyUpCommandProperty);
        }

        private static void KeyUpChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            TextBox element = target as TextBox;
            if (element != null)
            {
                if (e.OldValue != e.NewValue)
                element.PreviewKeyUp += Element_KeyUp;
            }
            else
            {
                element.PreviewKeyUp -= Element_KeyUp;
            }
        }

        private static void Element_KeyUp(object sender, KeyboardEventArgs e)
        {
            TextBox current = (TextBox)sender;
            BindingExpression be = current.GetBindingExpression(TextBox.TextProperty);

            if (be != null) be.UpdateSource();

            ICommand command = GetKeyUp(current);

            if ((command != null) && command.CanExecute(null))
                command.Execute(current);
        }

        #endregion TextBox KeyUp

    }
}




