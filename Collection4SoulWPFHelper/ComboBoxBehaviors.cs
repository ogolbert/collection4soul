﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;

namespace Golbert.WPFHelper.Behaviors
{

    public class ComboBoxBehaviors
    {
        //little usage for demonstration of behaviors and VisualTreeHelper, there are some others approaches for manipulate with background by actions 
        public static readonly DependencyProperty ComboBoxSelectionChangedProperty =
            DependencyProperty.RegisterAttached("ComboBoxSelectionChanged",
            typeof(ICommand),
            typeof(ComboBoxBehaviors),
            new UIPropertyMetadata(ComboBoxBehaviors.ComboBoxSelectionChangedPropertyChanged));

        public static ICommand GetComboBoxSelectionChanged(DependencyObject target)
        {
            return (ICommand)target.GetValue(ComboBoxSelectionChangedProperty);
        }

        public static void SetComboBoxSelectionChanged(DependencyObject target, ICommand value)
        {
            target.SetValue(ComboBoxSelectionChangedProperty, value);
        }

        private static void ComboBoxSelectionChangedPropertyChanged(DependencyObject target, DependencyPropertyChangedEventArgs e)
        {
            Selector element = target as Selector;
            if (element != null)
            {
                {
                    if ((e.NewValue != null) && (e.OldValue == null))
                        element.SelectionChanged += new SelectionChangedEventHandler(Element_SelectionChanged);
                    else if ((e.NewValue == null) && (e.OldValue != null))
                        element.SelectionChanged -= new SelectionChangedEventHandler(Element_SelectionChanged);
                }
            }
        }

        private static void Element_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox current = sender as ComboBox;
            //if (current.SelectionBoxItem != current.SelectedItem)
            //{
            //if ((current.SelectedItem != current.SelectionBoxItem) && (current.SelectionBoxItem != null))
            //{
            try
            {
                ICommand command = GetComboBoxSelectionChanged(current);
                if ((command != null) && (command.CanExecute(null)))
                    command.Execute(current);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
                //}
            //}
        }
    }
}

