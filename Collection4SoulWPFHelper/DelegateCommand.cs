﻿using System;
using System.Windows.Input;

namespace Golbert.WPFHelper
{
    public interface IDelegateCommand : ICommand
    {
        void RaiseCanExecuteChanges();
    }

    public sealed class DelegateCommand : IDelegateCommand
    {

        Action<object> execute;
        Func<object, bool> canExecute;

        public event EventHandler CanExecuteChanged;

        public DelegateCommand(Action<object> execute)
        {
            this.execute = execute;
            this.canExecute = this.AlwaysCanExecute;
        }

        public DelegateCommand(Action<object> execute, Func<object, bool> canExecute)
        {
            this.execute = execute;
            this.canExecute = canExecute;
        }

        public void Execute(object param)
        {
            this.execute(param);
        }


        public bool CanExecute(object param)
        {
            return canExecute(param);
        }


        public void RaiseCanExecuteChanges()
        {
            if (CanExecuteChanged != null)
                CanExecuteChanged(this, EventArgs.Empty);
        }

        private bool AlwaysCanExecute(object param)
        {
            return true;
        }
    }
}
