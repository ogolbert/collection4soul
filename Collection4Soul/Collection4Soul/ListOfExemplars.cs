﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using GolbertEnums = Golbert.Collection4Soul.Utilities.Enums;


namespace Golbert.Collection4Soul
{
    public class ListOfExemplars : List<Exemplar>
    {
        #region module variables

        XDocument _XDoc;

        #endregion module variables

        #region constructors

        public ListOfExemplars()
        {
            _XDoc = Golbert.Collection4Soul.Utilities.SingleTonDataFile.Instance.DataFile;

            var col = from c in _XDoc.Descendants("Exemplar") select new Exemplar()
            {
                ID = int.Parse(c.Attribute(GolbertEnums.PropertiesOfExemplars.ID.ToString()).Value),
                Name = c.Element(GolbertEnums.PropertiesOfExemplars.Name.ToString()).Value,
                Author = c.Element(GolbertEnums.PropertiesOfExemplars.Author.ToString()).Value,
                CreateYear = int.Parse(c.Element(GolbertEnums.PropertiesOfExemplars.CreateYear.ToString()).Value),
                Producer = c.Element(GolbertEnums.PropertiesOfExemplars.Producer.ToString()).Value,
                ProduceYear = int.Parse(c.Element(GolbertEnums.PropertiesOfExemplars.ProduceYear.ToString()).Value),
                Price = decimal.Parse(c.Element(GolbertEnums.PropertiesOfExemplars.Price.ToString()).Value),
                PriceMarket = decimal.Parse(c.Element(GolbertEnums.PropertiesOfExemplars.PriceMarket.ToString()).Value),
                Condition = (GolbertEnums.ConditionOfExemplars)Enum.Parse(typeof(GolbertEnums.ConditionOfExemplars), c.Element(GolbertEnums.PropertiesOfExemplars.Condition.ToString()).Value),
                Kind = (GolbertEnums.KindOfExemplar)Enum.Parse(typeof(GolbertEnums.KindOfExemplar), c.Element(GolbertEnums.PropertiesOfExemplars.Kind.ToString()).Value),
                Remark = c.Element(GolbertEnums.PropertiesOfExemplars.Remark.ToString()).Value
            };

            this.AddRange(col);
        }

        #endregion constructors
    }
}
