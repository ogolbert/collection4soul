﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Configuration;

namespace Golbert.Collection4Soul.Utilities
{
    public sealed class SingleTonID
    //delivers new values of ID for the class Exemplar. The last used value is saved in the XML-File IDGuard of this project
    {
        int _NewID;
        XDocument _XDoc;
        string _GuardFile = ConfigurationManager.AppSettings["IDGuard"];
        static SingleTonID instance;

        private SingleTonID ()
        {
             _XDoc = Golbert.Collection4Soul.Utilities.InternalCommonUsing.LoadXMLDocument(_GuardFile);
            GenerateNewIDFromFile();
        }

        public static SingleTonID Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SingleTonID();
                }
                return instance;
            }
        }

        public int NewID
        {
            get
            {
                GenerateNewIDFromFile();
                return _NewID;
            }
        }

        void GenerateNewIDFromFile()
        {
            //returning negativ means Error
            try
            {
                XElement item = _XDoc.Descendants("LastID").First();
                _NewID = int.Parse(item.Value) + 1;
                item.Value = _NewID.ToString();
                item.Save(_GuardFile);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                _NewID = -99;
            }
        }
    }
}
