﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Golbert.Collection4Soul.Utilities
{
    public static class DataFilePath
    {
        public static string Path = ConfigurationManager.AppSettings["DataFile"];
    }
}
