﻿using System.Xml.Linq;

namespace Golbert.Collection4Soul.Utilities
{
    public sealed class SingleTonDataFile
    {
        static XDocument datafile;
        static SingleTonDataFile instance;

        private SingleTonDataFile()
        {
            datafile = Golbert.Collection4Soul.Utilities.InternalCommonUsing.LoadXMLDocument(DataFilePath.Path);
        }

        public static SingleTonDataFile Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SingleTonDataFile();
                }
                return instance;
            }
        }

        public XDocument DataFile
        {
            get
            {
                return datafile;
            }
        }
    }
}
