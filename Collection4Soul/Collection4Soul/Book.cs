﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Golbert.Collection4Soul
{
    //not using
    //Class for Books, only Books printed on the paper. Inherited from nothing
    [DataContract]
    public class Book : Exemplar
    {
        #region module variables

        int _CountOfSites;

        #endregion module variables

        #region constructors

        public Book (string name) : base(name)
        {
        }

        public Book(string name, string author) : base(name, author)
        {
        }

        #endregion constructors

        #region public serializabled properties

        [DataMember]
        public int CountOfSites
        {
            set
            {
                _CountOfSites = value;
            }
            get
            {
                return _CountOfSites;
            }
        }

        #endregion public serializabled properties
    }
}
