﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Golbert.Collection4Soul.Utilities;

namespace Golbert.Collection4Soul
{
    //not using
    //Class for Video, inherited from nothing
    [DataContract]
    public class Audio : Multimedia
    {
        #region module variables

        Collection4Soul.Utilities.Enums.MultimediaMediaAudio _media;

        #endregion module variables

        #region constructors

        public Audio(string name) : base(name)
        {
        }

        public Audio(string name, string author) : base(name, author)
        {
        }

        #endregion constructors

        #region public serializabled properties

        [DataMember]
        public Collection4Soul.Utilities.Enums.MultimediaMediaAudio Media
        {
            set
            {
                _media = value;
            }
            get
            {
                return _media;
            }
        }

        #endregion public serializabled properties
    }
}
