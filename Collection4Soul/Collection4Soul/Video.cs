﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Golbert.Collection4Soul.Utilities;

namespace Golbert.Collection4Soul
{
    //not using
    //Class for Video, inherited from nothing
    [DataContract]
    public class Video : Multimedia
    {
        #region module variables

        Collection4Soul.Utilities.Enums.MultimediaMediaVideo _media;

        #endregion module variables

        #region constructors

        public Video(string name) : base(name)
        {
        }

        public Video(string name, string author) : base(name, author)
        {
        }

        #endregion constructors

        #region public serializabled properties

        [DataMember]
        public Collection4Soul.Utilities.Enums.MultimediaMediaVideo Media
        {
            set
            {
                _media = value;
            }
            get
            {
                return _media;
            }
        }

        public override bool Update(Exemplar saveme)
        {
            bool check = false;

            try
            {

            }
            catch (Exception ex)
            {
                check = false;
                Console.WriteLine(ex.Message);
            }
            return check;
        }

        #endregion public serializabled properties


    }
}
