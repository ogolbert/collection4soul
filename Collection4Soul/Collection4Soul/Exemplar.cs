﻿using System;
using System.Runtime.Serialization;
using System.Xml.Linq;
using System.Linq;
using GolbertEnums = Golbert.Collection4Soul.Utilities.Enums;

namespace Golbert.Collection4Soul
{
    //Basic class for Items of Collection, inherited from Classes Book und Multimedia
    //[KnownType(typeof(Audio))]
    //[KnownType(typeof(Video))]
    //[KnownType(typeof(Multimedia))]
    //[KnownType(typeof(Book))]
    [DataContract]
    public class Exemplar
    {
        #region module variables

        int _ID;
        string _name;
        string _author;
        int _createYear;     
        string _producer;
        int _produceYear;
        string _remark;
        decimal _price;
        decimal _priceMarket;
        Collection4Soul.Utilities.Enums.KindOfExemplar _kind;
        Collection4Soul.Utilities.Enums.ConditionOfExemplars _condition;

        XDocument _XDoc;

        bool _isNew = false;
        #endregion module variables


        #region constructors
        public Exemplar()
        {
            _XDoc = Golbert.Collection4Soul.Utilities.SingleTonDataFile.Instance.DataFile;
        }

        public Exemplar(bool isNew) : this()
        {
            _isNew = isNew;
        }


        public Exemplar(string name) : this()
        {
            _name = name;
        }

        public Exemplar (string name, string author) : this(name)
        {
            _author = author;
        }

        public Exemplar(string name, string author, int produseYear) : this(name)
        {
            _author = author;
            _produceYear = produseYear;
        }

        #endregion constructors

        #region public serializabled properties
        [DataMember]
        public int ID
        {
            internal set
            {
                //delivered from internal single ton
                if (_isNew) _ID = Golbert.Collection4Soul.Utilities.SingleTonID.Instance.NewID;
                else _ID = value;
            }
            get
            {
                return _ID;
            }
        }

        public bool IsNew
        {
            set
            {
                _isNew = value;
            }
            get
            {
                return _isNew;
            }
        }

        [DataMember]
        public string Name
        {
            set
            {
                _name = value;
            }
            get
            {
                return _name;
            }
        }

        [DataMember]
        public string Author
        {
            set
            {
                _author = value;
            }
            get
            {
                return _author;
            }
        }

        [DataMember]
        public int CreateYear
        {
            set
            {
                _createYear = value;
            }
            get
            {
                return _createYear;
            }
        }

        [DataMember]
        public string Producer
        {
            set
            {
                _producer = value;
            }
            get
            {
                return _producer;
            }
        }

        [DataMember]
        public int ProduceYear
        {
            set
            {
                _produceYear = value;
            }
            get
            {
                return _produceYear;
            }
        }

        [DataMember]
        public string Remark
        {
            set
            {
                _remark = value;
            }
            get
            {
                return _remark;
            }
        }

        [DataMember]
        public decimal Price
        {
            set
            {
                _price = value;
            }
            get
            {
                return _price;
            }
        }

        [DataMember]
        public decimal PriceMarket
        {
            set
            {
                _priceMarket = value;
            }
            get
            {
                return _priceMarket;
            }
        }

        [DataMember]
        public Collection4Soul.Utilities.Enums.ConditionOfExemplars Condition
        {
            set
            {
                _condition = value;
            }
            get
            {
                return _condition;
            }
        }

        [DataMember]
        public Collection4Soul.Utilities.Enums.KindOfExemplar Kind
        {
            set
            {
                _kind = value;
            }
            get
            {
                return _kind;
            }
        }

        #endregion public serializabled properties

        #region public methods

        public virtual int Insert(Exemplar saveme)
        {
            XElement exemp;
            try
            {
                exemp = 
                    new XElement("Exemplar", 
                    new XAttribute(GolbertEnums.PropertiesOfExemplars.ID.ToString(), saveme.ID), 
                    new XElement(GolbertEnums.PropertiesOfExemplars.Name.ToString(), saveme.Name),
                    new XElement(GolbertEnums.PropertiesOfExemplars.Author.ToString(), saveme.Author), 
                    new XElement(GolbertEnums.PropertiesOfExemplars.CreateYear.ToString(), saveme.CreateYear), 
                    new XElement(GolbertEnums.PropertiesOfExemplars.Producer.ToString(), saveme.Producer), 
                    new XElement(GolbertEnums.PropertiesOfExemplars.ProduceYear.ToString(), saveme.ProduceYear),
                    new XElement(GolbertEnums.PropertiesOfExemplars.Remark.ToString(), saveme.Remark), 
                    new XElement(GolbertEnums.PropertiesOfExemplars.Price.ToString(), saveme.Price), 
                    new XElement(GolbertEnums.PropertiesOfExemplars.PriceMarket.ToString(), saveme.PriceMarket), 
                    new XElement(GolbertEnums.PropertiesOfExemplars.Condition.ToString(), saveme.Condition), 
                    new XElement(GolbertEnums.PropertiesOfExemplars.Kind.ToString(), saveme.Kind));
                _XDoc.Root.Add(exemp);
                _XDoc.Save(Utilities.DataFilePath.Path);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return saveme.ID;
        }

        public virtual bool Update(Exemplar saveme)
        {
            bool check = true;
            XElement exemp;

            try
            {
                {
                    exemp = _XDoc.Descendants("Exemplar").FirstOrDefault
                        (x => x.Attribute(GolbertEnums.PropertiesOfExemplars.ID.ToString()).Value == saveme.ID.ToString());
                    exemp.Element(GolbertEnums.PropertiesOfExemplars.Name.ToString()).SetValue(saveme.Name);
                    exemp.Element(GolbertEnums.PropertiesOfExemplars.Author.ToString()).SetValue(saveme.Author);
                    exemp.Element(GolbertEnums.PropertiesOfExemplars.CreateYear.ToString()).SetValue(saveme.CreateYear);
                    exemp.Element(GolbertEnums.PropertiesOfExemplars.Producer.ToString()).SetValue(saveme.Producer);
                    exemp.Element(GolbertEnums.PropertiesOfExemplars.ProduceYear.ToString()).SetValue(saveme.ProduceYear);
                    exemp.Element(GolbertEnums.PropertiesOfExemplars.Remark.ToString()).SetValue(saveme.Remark);
                    exemp.Element(GolbertEnums.PropertiesOfExemplars.Price.ToString()).SetValue(saveme.Price);
                    exemp.Element(GolbertEnums.PropertiesOfExemplars.PriceMarket.ToString()).SetValue(saveme.PriceMarket);
                    exemp.Element(GolbertEnums.PropertiesOfExemplars.Condition.ToString()).SetValue(saveme.Condition);
                    exemp.Element(GolbertEnums.PropertiesOfExemplars.Kind.ToString()).SetValue(saveme.Kind);
                    _XDoc.Save(Utilities.DataFilePath.Path);
                }
            }
            catch (Exception ex)
            {
                check = false;
                Console.WriteLine(ex.Message);
            }
            return check;
        }
        #endregion public methods
    }
}
