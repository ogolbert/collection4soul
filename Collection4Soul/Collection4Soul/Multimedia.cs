﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;


namespace Golbert.Collection4Soul
{
    //not using
    //common Class for Audio/Video, inherits from Audio and Video

    //[KnownType (typeof(Audio))]
    //[KnownType(typeof(Video))]
    [DataContract]
    public class Multimedia : Exemplar
    {
        #region module variables

        decimal _PlayTime;

        #endregion module variables

        #region constructors

        public Multimedia (string name) : base(name)
        {
        }

        public Multimedia (string name, string author) : base(name, author)
        {
        }

        #endregion constructors

        #region public serializabled properties

        [DataMember]
        public decimal PlayTime
        {
            set
            {
                _PlayTime = value;
            }
            get
            {
                return _PlayTime;
            }
        }

        #endregion public serializabled properties
    }
}
