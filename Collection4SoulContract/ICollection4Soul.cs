﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Xml.Serialization;

namespace Golbert.Collection4Soul.Contract
{
    //[KnownType(typeof(Audio))]
    //[KnownType(typeof(Video))]
    //[KnownType(typeof(Multimedia))]
    //[KnownType(typeof(Book))]
    [ServiceContract]
    public interface ICollection4SoulContract
    {
        [OperationContract]
        bool UpdateExemplar(string xml);

        [OperationContract]
        int InsertExemplar(string xml);

        [OperationContract]
        XmlSerializer ReadExemplar(int id);

        [OperationContract]
        string[] GetExemplars();
    }
}
