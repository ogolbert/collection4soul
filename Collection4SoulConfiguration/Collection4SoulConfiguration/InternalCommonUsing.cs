﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;
using System.Reflection;
using GolbertEnums = Golbert.Collection4Soul.Utilities.Enums;


namespace Golbert.Collection4Soul.Utilities
{
    public static class InternalCommonUsing
    {
        public static XDocument LoadXMLDocument(string file2Open)
        {
            try
            {
                XDocument doc = XDocument.Load(file2Open);
                return doc;
            }
            catch (Exception ex)
            {
                throw new NotImplementedException();
            }
        }


        #region serialize list
        public static List<string> SerializeList<T>(List<T> sourceList)
        {
            List<string> result = new List<string>();

            foreach (var x in sourceList)
            {
                result.Add(Serialize(x));
            }
            return result;
        }


        public static string Serialize<T>(T source)
        {
            string xml = "";

            try
            {
                using (MemoryStream memoryStream = new MemoryStream())
                using (StreamReader reader = new StreamReader(memoryStream))
                {
                    DataContractSerializer serializer = new DataContractSerializer(source.GetType());
                    serializer.WriteObject(memoryStream, source);
                    memoryStream.Position = 0;
                    xml = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace + " ::: " + ex.Message);
                xml = "wrong xml: " + ex.Message;
            }
            return xml;
        }
        #endregion serialize list

        #region deserialize to some type (by Collection4Soul not used)
        public static T Deserialize<T>(string xml, Type toType)
        {
            object result = null;
            try
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    byte[] data = System.Text.Encoding.UTF8.GetBytes(xml);
                    stream.Write(data, 0, data.Length);
                    stream.Position = 0;

                    DataContractSerializer deserializer = new DataContractSerializer(toType);

                    result = deserializer.ReadObject(stream);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace + " ::: " + ex.Message);
            }
            return (T)result;
        }
        #endregion deserialize to some type 

        #region serialize by two types 


        public static void CreateObjFromXML<T>(string xml, T result)
        {
            //designed and works just in case if names of properties an both sides are identical
            try
            {
                using (XmlReader reader = XmlReader.Create(new StringReader(xml)))
                {
                    try
                    {
                        reader.MoveToContent();
                        reader.Read();

                        while (!(reader.NodeType == XmlNodeType.EndElement))
                        {
                            if (reader.NodeType == XmlNodeType.Element)
                            {
                                FillPropertiesFromXML(reader.Name, reader.ReadInnerXml(), result);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.Write(ex.Source + ":::" + ex.Data + ":::" + ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException);
            }
        }

        static void FillPropertiesFromXML<T> (string propName, string xmlValue, T processed)
        {
            try
            {
                PropertyInfo info = processed.GetType().GetProperty(propName);

                if (!info.GetMethod.ReturnType.IsEnum)
                    // standard datatypes: primitives & Co. Not self-difined Enums
                    // included just datatypes using in this project 
                {
                    TypeCode ere = Type.GetTypeCode(info.GetMethod.ReturnType);
                    switch (ere)
                    {
                        case TypeCode.String:
                            info.SetValue(processed, xmlValue);
                            break;
                        case TypeCode.Int32:
                            info.SetValue(processed, int.Parse(xmlValue));
                            break;
                        case TypeCode.Decimal:
                            info.SetValue(processed, decimal.Parse(xmlValue));
                            break;
                        default:
                            //datatype off this project not included and hendling as string. 
                            //It can be expandet, for example:
                            //case TypeCode.Double:
                            //info.SetValue(processed, double.Parse(xmlValue));
                            //break;
                            info.SetValue(processed, xmlValue);
                            break;
                    }
                }
                else
                //for this project self-defined Enums
                {
                    string type = info.GetMethod.ReturnType.Name;

                    if (type == typeof(GolbertEnums.ConditionOfExemplars).Name) info.SetValue(processed, Enum.Parse(typeof(GolbertEnums.ConditionOfExemplars), xmlValue));
                    else if (type == typeof(GolbertEnums.KindOfExemplar).Name) info.SetValue(processed, Enum.Parse(typeof(GolbertEnums.KindOfExemplar), xmlValue));
                    else info.SetValue(processed, xmlValue);
                }
                //Console.WriteLine("by exemplar: " + info.Name + " = " + info.GetValue(processed));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        #endregion serialize by two types (property names are identic)
    }
}
