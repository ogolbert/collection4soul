﻿using System.ComponentModel;

namespace Golbert.Collection4Soul.Utilities.Enums
{
    #region for Class Exemplar

    public enum KindOfExemplar
    {
        [Description("Buch, Zeitschrift, Zeitung")]
        Literature = 0,
        [Description("Multimedia audio")]
        Music,
        [Description("Multimedia video")]
        Video
    };

    public enum ConditionOfExemplars
    {
        [Description("sehr gut")]
        Einwandfrei = 0,
        [Description("einfach gut")]
        Gut,
        [Description("geht so")]
        Befriedigend,
        [Description("nicht zufrieden")]
        Schlecht,
        [Description("alles schlecht")]
        Schräcklich
    };

    public enum PropertiesOfExemplars
    {
        ID = 0,
        Name,
        Author,
        CreateYear,
        Producer,
        ProduceYear,
        Price,
        PriceMarket,
        Condition,
        Kind,
        Remark
    };

    #endregion for Class Exemplar

    //not using
    #region for Class Audio
    public enum MultimediaMediaAudio
    {
        [Description("Kompaktdisk")]
        CD = 0,
        [Description("Schalplatte")]
        LP,
        [Description("Single 45''")]
        Single,
        [Description("Bandaufnahme")]
        Audiotape,
        [Description("Audiokasette")]
        Audiocassete
    };
    #endregion for Class Audio

    //not using
    #region for Class Media
    public enum MultimediaMediaVideo
    {
        [Description("Kompaktdisk")]
        CD = 0,
        [Description("DVD Disk")]
        DVD,
        [Description("Blu Ray")]
        BluRay,
        [Description("Videoband")]
        Videotape,
        [Description("Viedeokasette")]
        Videocassete
    };
    #endregion for Class Media
}
