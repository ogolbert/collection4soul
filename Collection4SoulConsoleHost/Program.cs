﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Golbert.Collection4Soul.Service;

namespace Golbert.Collection4Soul.Host
{
    class Program
    {
        static void Main()
        {
            using (var host = new ServiceHost(typeof(Golbert.Collection4Soul.Service.Collection4SoulService)))
            {
                host.Open();

                Console.WriteLine("Service of Collection4Soul running");
                Console.ReadLine();
            }
        }
    }
}
