﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.IO;

namespace Golbert.Collection4Soul.Clients
{
    /// <summary>
    /// Interaktionslogik für "App.xaml"
    /// </summary>
    public partial class App : Application
    {
        //private static string _directoryPath = System.Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Collection4SoulLogs\\";
        //private static string _logPath = _directoryPath + System.DateTime.Today.ToString("yyyyMMdd") + System.DateTime.Now.ToString("HHmmss") + "_" + typeof(App).Assembly.FullName + ".log";

        public App()
        {
            Dispatcher.UnhandledException += Dispatcher_UnhandledException;
        }

        private void Dispatcher_UnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            var errorMessage = string.Format("An exception occurred: {0}", e.Exception.Message);
            MessageBox.Show(errorMessage, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            Logger.writeLog(e.Exception);
            e.Handled = true;
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            var viewmodel = new Collection4Soul.Clients.ViewModel.Collection4SoulViewVM();
            viewmodel.ErrorFromMainVM += Viewmodel_ErrorFromMainVM;
            Logger.createLog();
            MainWindow mw = new MainWindow();
            mw.DataContext = viewmodel;
            mw.Show();
        }

        private void Viewmodel_ErrorFromMainVM(object sender, ViewModel.ErrorEventArgs e)
        {
            Logger.writeLog(e.ErrorData);
        }

        //public static void WriteLog()
        //{

        //}

        //void createLog()
        //{
        //    string directoryPath = System.Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Collection4SoulLogs\\";
        //    if (!Directory.Exists(directoryPath)) Directory.CreateDirectory(directoryPath);

        //    writeLogInfo("Started without errors " + System.DateTime.Today.ToString("yyyy.MM.dd") + " at " + System.DateTime.Now.ToString("HH:mm:ss"));
        //}

        //void writeLogInfo(string info)
        //{
        //    using (StreamWriter writer = File.AppendText(_logPath))
        //    {
        //        writer.WriteLine(info);
        //    }
        //}
        //void writeLog(Exception exeption)
        //{
        //    using (StreamWriter writer = File.AppendText(_logPath))
        //    {
        //        writer.WriteLine(">>>Occured: " + System.DateTime.Today.ToString("yyyy.MM.dd") + " at " + System.DateTime.Now.ToString("HH:mm:ss"));
        //        writer.WriteLine(">>Message: " + exeption.GetBaseException().Message.ToString());
        //        if (exeption.InnerException != null) writer.WriteLine(">>InnerExeption: " + exeption.InnerException.Message.ToString());
        //        writer.WriteLine(">>StackTrace: " + exeption.StackTrace.ToString());
        //    }
        //}
    }
}
