﻿//using System;
//using System.IO;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace Golbert.Collection4Soul.Clients.ViewModel
//{
//    public interface IErrorServiceVM
//    {
//        event EventHandler<ErrorEventArgs> ErrorFromMainVM;
//    }
//    public interface ILogger
//    {
//        void writeLog(Exception ex);
//    }

//    public class ErrorServiceVM : IErrorServiceVM
//    {
//        public event EventHandler<ErrorEventArgs> ErrorFromMainVM;
//    }

//    public class LoggerA : ILogger
//    {
//        private static string _directoryPath = System.Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Collection4SoulLogs\\";
//        private static string _logPath = _directoryPath + System.DateTime.Today.ToString("yyyyMMdd") + System.DateTime.Now.ToString("HHmmss") + "_" + typeof(App).Assembly.FullName + ".log";

//        public void writeLog(Exception exeption)
//        {
//            using (StreamWriter writer = File.AppendText(_logPath))
//            {
//                writer.WriteLine(">>>Occured: " + System.DateTime.Today.ToString("yyyy.MM.dd") + " at " + System.DateTime.Now.ToString("HH:mm:ss"));
//                writer.WriteLine(">>Message: " + exeption.GetBaseException().Message.ToString());
//                if (exeption.InnerException != null) writer.WriteLine(">>InnerExeption: " + exeption.InnerException.Message.ToString());
//                writer.WriteLine(">>StackTrace: " + exeption.StackTrace.ToString());
//            }
//        }
//    }

//    public class Abstracter 
//    {
//        IErrorServiceVM _error;
//        ILogger _logger;

//        public Abstracter(IErrorServiceVM error, ILogger logger)
//        {
//            _error = error;
//            _logger = logger;

//            _error.ErrorFromMainVM += Error_ErrorFromMainVM;
//        }

//        private void Error_ErrorFromMainVM(object sender, ErrorEventArgs e)
//        {
//            _logger.writeLog(e.ErrorData);
//        }
//    }
//}
