﻿using System;
using System.Windows;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Golbert.Collection4Soul.Clients
{
    public static class Logger
    {
        private static string _directoryPath = System.Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Collection4SoulLogs\\";
        private static string _logPath = _directoryPath + System.DateTime.Today.ToString("yyyyMMdd") + System.DateTime.Now.ToString("HHmmss") + "_" + typeof(App).Assembly.FullName + ".log";

        public static void createLog()
        {
            string directoryPath = System.Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\Collection4SoulLogs\\";
            if (!Directory.Exists(directoryPath)) Directory.CreateDirectory(directoryPath);

            writeLog("Started without errors " + System.DateTime.Today.ToString("yyyy.MM.dd") + " at " + System.DateTime.Now.ToString("HH:mm:ss"));
        }
        public static void  writeLog(string info)
        {
            using (StreamWriter writer = File.AppendText(_logPath))
            {
                writer.WriteLine(info);
            }
        }
        public static void writeLog(Exception exeption)
        {
            using (StreamWriter writer = File.AppendText(_logPath))
            {
                writer.WriteLine(">>>Occured: " + System.DateTime.Today.ToString("yyyy.MM.dd") + " at " + System.DateTime.Now.ToString("HH:mm:ss"));
                writer.WriteLine(">>Message: " + exeption.GetBaseException().Message.ToString());
                if (exeption.InnerException != null) writer.WriteLine(">>InnerExeption: " + exeption.InnerException.Message.ToString());
                writer.WriteLine(">>StackTrace: " + exeption.StackTrace.ToString());
            }
        }
    }
}
